import React from 'react';

const Category = ({id, title, image}) => {
  return (
    <a href={`/shop/${id}`}>
        <div className="display-container" style={{ boxShadow: '0 2px 7px #dfdfdf', display: 'flex', alignItems: 'center', justifyContent: 'center', height: '300px', padding: '80px' }}>
            <div className="display-topleft black padding">{title}</div>
            <img src={image} alt="House" style={{ maxWidth: '100%', minHeight: '100%' }} />
        </div>
    </a>
  );
}

export default Category;
