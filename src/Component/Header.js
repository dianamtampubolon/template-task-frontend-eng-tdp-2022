import React from 'react';

function Header() {
  return <div>
    <div className="top" >
        <div className="bar white wide padding card">
            <a href="/" className="bar-item button"><b>EDTS</b> TDP Batch #2</a>
            <div className="right hide-small">
                <a href="/" className="bar-item button">Home</a>
                <a href="/shop" className="bar-item button">Shop</a>
            </div>
        </div>
    </div>
  </div>;
}

export default Header;
