import React from 'react';

function ProductCategory() {
  return (
    <a href={`/shop?category=1`}>
        <div className="display-container" style={{ boxShadow: '0 2px 7px #dfdfdf', display: 'flex', alignItems: 'center', justifyContent: 'center', height: '300px', padding: '80px' }}>
            <div className="display-topleft black padding">Electronic</div>
            <img src="https://fakestoreapi.com/img/81QpkIctqPL._AC_SX679_.jpg" alt="House" style={{ maxWidth: '100%', minHeight: '100%' }} />
        </div>
    </a>
  );
}

export default ProductCategory;
