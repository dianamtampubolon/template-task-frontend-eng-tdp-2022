import React, {useEffect, useState} from 'react';
import axios from 'axios'
import { useParams } from 'react-router-dom'


const Shop = () => {
    let {id} = useParams()
    const [productList, setProductList] = useState([]);
    const [isLoad, setIsLoad] = useState(true)
    useEffect(async () => {
        const result = await axios(
          'https://api-tdp-2022.vercel.app/api/products',
        );
        setProductList(result.data.data.productList);
        setIsLoad(false)     
    },[]);
    let productFilter = productList.filter( x => x.category_id === parseInt(id))
    console.log(productFilter)
  return (
    <div className="content padding" style={{ maxWidth: '1564px' }}>
        <div className="container" style={{ marginTop: '80px' }}>
            <div className='breadcrumb'>
                <span>
                    <a href='/'>Home</a>
                    <span className="breadcrumb-separator">/</span>
                </span>
                <span>
                    <a href="/">Shop</a>
                </span>
            </div>
        </div>
        <div className="container padding-32" id="about">
            <h3 className="border-bottom border-light-grey padding-16">
                All Product
            </h3>
        </div>
        {isLoad ? <div className="row-padding padding-large" style={{ fontSize: '16px', fontWeight: '800', textAlign: 'center' }}>
             Loading . . .
         </div>
         : 
        <div className="row-padding rm-before-after" style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
            {productFilter.map(product => (
                <div className="product-card" key={product.id}>
                    <div className="badge">{product.discount}</div>
                    <div className="product-tumb">
                        <img src={product.image} alt="product1" />
                    </div>
                    <div className="product-details">
                        <span className="product-catagory">{product.category}</span>
                        <h4>
                            <a href={`/shop/detail/${product.id}`}>{product.title}</a>
                        </h4>
                        <p>{product.description}</p>
                        <div className="product-bottom-details">
                            <div className="product-price">Rp. {product.price}</div>
                            <div className="product-links">
                                <a href={`/shop/detail/${product.id}`}>View Detail<i className="fa fa-heart"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                ))}
        </div>
}
    </div>
  );
}

export default Shop;
