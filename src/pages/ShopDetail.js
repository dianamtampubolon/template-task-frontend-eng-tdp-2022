import React, {useEffect, useState} from 'react';
import axios from 'axios'
import { useParams } from 'react-router-dom'

function ShopDetail() {
  let id = useParams()
  console.log(typeof(id))
  const [detail, setDetail] = useState([]);
    const [isLoad, setIsLoad] = useState(true)
    useEffect(async () => {
        const result = await axios(
          `https://api-tdp-2022.vercel.app/api/products/${parseInt(id.id)}`,
        );
        setDetail(result.data.data);
        setIsLoad(false)     
    },[]);
    console.log(detail)
  return <div>
    <div className="content padding" style={{ maxWidth: '1564px' }}>
        <div className="container" style={{ marginTop: '80px' }}>
            <div className='breadcrumb'>
                <span>
                    <a href='/'>Home</a>
                    <span className="breadcrumb-separator">/</span>
                </span>
                <span>
                    <a href="/">Shop</a>
                    <span className="breadcrumb-separator">/</span>
                    <a href="/">Detail</a>
                </span>
            </div>
        </div>
        <div className="container padding-32" id="about">
            <h3 className="border-bottom border-light-grey padding-16">Product Information</h3>
        </div>
        {
        <div className="row-padding card card-shadow padding-large" style={{ marginTop: '50px' }}>
            <div className="col l3 m6 margin-bottom">
                <div className="product-tumb">
                    <img src="https://fakestoreapi.com/img/71-3HjGNDUL._AC_SY879._SX._UX._SY._UY_.jpg" alt="Product 1" />
                </div>
            </div>
            <div className="col m6 margin-bottom">
                <h3>{detail.title}</h3>
                <div style={{ marginBottom: '32px' }}>
                    <span>Category : <strong>{detail.category}</strong></span>
                    <span style={{ marginLeft: '30px' }}>Review : <strong>{detail.rate}</strong></span>
                    <span style={{ marginLeft: '30px' }}>Stock : <strong>{detail.stock}</strong></span>
                    <span style={{ marginLeft: '30px' }}>Discount : <strong>{detail.discount} %</strong></span>
                </div>
                <div style={{ fontSize: '2rem', lineHeight: '34px', fontWeight: '800', marginBottom: '32px' }}>
                    Rp. {detail.price}
                </div>
                <div style={{ marginBottom: '32px' }}>
                    {detail.description}
                </div>
                <div style={{ marginBottom: '32px' }}>
                    <div><strong>Quantity : </strong></div>
                    <input type="number" className="input section border" name="total" placeholder='Quantity' />
                </div>
                <div style={{ marginBottom: '32px', fontSize: '2rem', fontWeight: 800 }}>
                    Sub Total : Rp. {detail.price}
                    <span style={{ marginLeft: '30px', fontSize: '18px', textDecoration: 'line-through' }}><strong>Rp. 350.000</strong></span>
                </div>
                <button className='button light-grey block' >Add to cart</button>
                <button className='button light-grey block' disabled={true}>Empty Stock</button>
            </div>
        </div>
        }
    </div>
  </div>;
}

export default ShopDetail;
